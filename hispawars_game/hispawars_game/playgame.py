import sys
import os
import json
from io import StringIO
import hispawars_viz_aichallenge
from .ants import Ants
from .engine import run_game
from .colorize import stderr


class Comment(object):
    def __init__(self, file):
        self.file = file
        self.last_char = "\n"

    def write(self, data):
        for char in data:
            if self.last_char == "\n":
                self.file.write("# ")
            self.file.write(char)
            self.last_char = char

    def flush(self):
        self.file.flush()

    def close(self):
        self.file.close()


class Tee(object):
    """ Write to multiple files at once """

    def __init__(self, *files):
        self.files = files

    def write(self, data):
        for file in self.files:
            file.write(data)

    def flush(self):
        for file in self.files:
            file.flush()

    def close(self):
        for file in self.files:
            file.close()


def run_rounds(opts, args):
    def get_cmd_wd(cmd, exec_rel_cwd=False):
        """ get the proper working directory from a command line """
        new_cmd = []
        wd = None
        for i, part in reversed(list(enumerate(cmd.split()))):
            if wd is None and os.path.exists(part):
                wd = os.path.dirname(os.path.realpath(part))
                basename = os.path.basename(part)
                if i == 0:
                    if exec_rel_cwd:
                        new_cmd.insert(0, os.path.join(".", basename))
                    else:
                        new_cmd.insert(0, part)
                else:
                    new_cmd.insert(0, basename)
            else:
                new_cmd.insert(0, part)
        return wd, " ".join(new_cmd)

    def get_cmd_name(cmd):
        """ get the name of a bot from the command line """
        for i, part in enumerate(reversed(cmd.split())):
            if os.path.exists(part):
                return os.path.basename(part)

    # this split of options is not needed, but left for documentation
    game_options = {
        "map": opts.map,
        "attack": opts.attack,
        "kill_points": opts.kill_points,
        "food": opts.food,
        "viewradius2": opts.viewradius2,
        "attackradius2": opts.attackradius2,
        "spawnradius2": opts.spawnradius2,
        "loadtime": opts.loadtime,
        "turntime": opts.turntime,
        "turns": opts.turns,
        "food_rate": opts.food_rate,
        "food_turn": opts.food_turn,
        "food_start": opts.food_start,
        "food_visible": opts.food_visible,
        "cutoff_turn": opts.cutoff_turn,
        "cutoff_percent": opts.cutoff_percent,
        "scenario": opts.scenario,
    }
    if opts.player_seed is not None:
        game_options["player_seed"] = opts.player_seed
    if opts.engine_seed is not None:
        game_options["engine_seed"] = opts.engine_seed
    engine_options = {
        "loadtime": opts.loadtime,
        "turntime": opts.turntime,
        "map_file": opts.map,
        "turns": opts.turns,
        "log_replay": opts.log_replay,
        "log_stream": opts.log_stream,
        "log_input": opts.log_input,
        "log_output": opts.log_output,
        "log_error": opts.log_error,
        "serial": opts.serial,
        "strict": opts.strict,
        "capture_errors": opts.capture_errors,
        "secure_jail": opts.secure_jail,
        "end_wait": opts.end_wait,
    }
    for round in range(opts.rounds):
        # initialize game
        game_id = round + opts.game_id
        with open(opts.map, "r") as map_file:
            game_options["map"] = map_file.read()
        if opts.engine_seed:
            game_options["engine_seed"] = opts.engine_seed + round
        game = Ants(game_options)
        # initialize bots
        bots = [get_cmd_wd(arg, exec_rel_cwd=opts.secure_jail) for arg in args]
        bot_count = len(bots)
        # insure correct number of bots, or fill in remaining positions
        if game.num_players != len(bots):
            if game.num_players > len(bots) and opts.fill:
                extra = game.num_players - len(bots)
                for _ in range(extra):
                    bots.append(bots[-1])
            else:
                print(
                    "Incorrect number of bots for map.  Need {0}, got {1}".format(
                        game.num_players, len(bots)
                    ),
                    file=stderr,
                )
                for arg in args:
                    print("Bot Cmd: {0}".format(arg), file=stderr)
                break
        bot_count = len(bots)
        # move position of first bot specified
        if opts.position > 0 and opts.position <= len(bots):
            first_bot = bots[0]
            bots = bots[1:]
            bots.insert(opts.position, first_bot)

        # initialize file descriptors
        if opts.log_dir and not os.path.exists(opts.log_dir):
            os.mkdir(opts.log_dir)
        if (
            not opts.log_replay and not opts.log_stream and (opts.log_dir or opts.log_stdout)
        ):
            opts.log_replay = True
        replay_path = None  # used for visualizer launch

        if opts.log_replay:
            if opts.log_dir:
                replay_path = os.path.join(opts.log_dir, "{0}.replay".format(game_id))
                engine_options["replay_log"] = open(replay_path, "w")
            if opts.log_stdout:
                if "replay_log" in engine_options and engine_options["replay_log"]:
                    engine_options["replay_log"] = Tee(
                        sys.stdout, engine_options["replay_log"]
                    )
                else:
                    engine_options["replay_log"] = sys.stdout
        else:
            engine_options["replay_log"] = None

        if opts.log_stream:
            if opts.log_dir:
                engine_options["stream_log"] = open(
                    os.path.join(opts.log_dir, "{0}.stream".format(game_id)), "w"
                )
            if opts.log_stdout:
                if engine_options["stream_log"]:
                    engine_options["stream_log"] = Tee(
                        sys.stdout, engine_options["stream_log"]
                    )
                else:
                    engine_options["stream_log"] = sys.stdout
        else:
            engine_options["stream_log"] = None

        if opts.log_input and opts.log_dir:
            engine_options["input_logs"] = [
                open(
                    os.path.join(opts.log_dir, "{0}.bot{1}.input".format(game_id, i)),
                    "w",
                )
                for i in range(bot_count)
            ]
        else:
            engine_options["input_logs"] = None
        if opts.log_output and opts.log_dir:
            engine_options["output_logs"] = [
                open(
                    os.path.join(opts.log_dir, "{0}.bot{1}.output".format(game_id, i)),
                    "w",
                )
                for i in range(bot_count)
            ]
        else:
            engine_options["output_logs"] = None
        if opts.log_error and opts.log_dir:
            if opts.log_stderr:
                if opts.log_stdout:
                    engine_options["error_logs"] = [
                        Tee(
                            Comment(stderr),
                            open(
                                os.path.join(
                                    opts.log_dir, "{0}.bot{1}.error".format(game_id, i)
                                ),
                                "w",
                            ),
                        )
                        for i in range(bot_count)
                    ]
                else:
                    engine_options["error_logs"] = [
                        Tee(
                            stderr,
                            open(
                                os.path.join(
                                    opts.log_dir, "{0}.bot{1}.error".format(game_id, i)
                                ),
                                "w",
                            ),
                        )
                        for i in range(bot_count)
                    ]
            else:
                engine_options["error_logs"] = [
                    open(
                        os.path.join(
                            opts.log_dir, "{0}.bot{1}.error".format(game_id, i)
                        ),
                        "w",
                    )
                    for i in range(bot_count)
                ]
        elif opts.log_stderr:
            if opts.log_stdout:
                engine_options["error_logs"] = [Comment(stderr)] * bot_count
            else:
                engine_options["error_logs"] = [stderr] * bot_count
        else:
            engine_options["error_logs"] = None

        if opts.verbose:
            if opts.log_stdout:
                engine_options["verbose_log"] = Comment(sys.stdout)
            else:
                engine_options["verbose_log"] = sys.stdout

        engine_options["game_id"] = game_id
        if opts.rounds > 1:
            print("# playgame round {0}, game id {1}".format(round, game_id))

        # intercept replay log so we can add player names
        if opts.log_replay:
            intcpt_replay_io = StringIO()
            real_replay_io = engine_options["replay_log"]
            engine_options["replay_log"] = intcpt_replay_io

        run_game(game, bots, engine_options)

        # add player names, write to proper io, reset back to normal
        if opts.log_replay:
            replay_json = json.loads(intcpt_replay_io.getvalue())
            replay_json["playernames"] = [get_cmd_name(arg) for arg in args]
            real_replay_io.write(json.dumps(replay_json))
            intcpt_replay_io.close()
            engine_options["replay_log"] = real_replay_io

        # close file descriptors
        if engine_options["stream_log"]:
            engine_options["stream_log"].close()
        if engine_options["replay_log"]:
            engine_options["replay_log"].close()
        if engine_options["input_logs"]:
            for input_log in engine_options["input_logs"]:
                input_log.close()
        if engine_options["output_logs"]:
            for output_log in engine_options["output_logs"]:
                output_log.close()
        if engine_options["error_logs"]:
            for error_log in engine_options["error_logs"]:
                error_log.close()
        if replay_path:
            if opts.nolaunch:
                if opts.html_file:
                    hispawars_viz_aichallenge.launch(replay_path, True, opts.html_file)
            else:
                if opts.html_file is None:
                    hispawars_viz_aichallenge.launch(
                        replay_path, generated_path="replay.{0}.html".format(game_id)
                    )
                else:
                    hispawars_viz_aichallenge.launch(replay_path, generated_path=opts.html_file)
