# Bienvenidos a Hispawars

Este proyecto nace con el objetivo principal de generar interés por la programación y atraer a quienes ya tengan un recorrido hecho para que sigan aprendiendo por medio de una competencia.

Buscamos crear una plataforma para enseñar programación que pueda servir en última instancia a organismos educativos (aunque de ningún modo es un límite).

Lo más importante: **enseñar y aprender** programación **divirtiéndose**! :smile:

## Welcome to Hispawars!
Get to the [english version](/en)
