#!/usr/bin/env sh
python -m hispawars_game -e \
                         --capture_errors \
                         --end_wait=0.25 \
                         --verbose \
                         --log_dir /tmp/logs \
                         --turns 300 \
                         --map_file sample.map \
                         "python -m hispawars_bot" \
                         "python -m hispawars_bot"
